package aula;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class OperacaoTest {
	private Operacao operacao;
	/** � chamado antes de cada m�todo de teste */
	@Before
	public void setUp() {
		operacao = new Operacao();
	}
	/** � chamado ap�s cada m�todo de teste para limpar o lixo */
	@After
	public void tearDown() {
		operacao = null;
	}
	@Test
	public void testPotencia() {
		assertEquals("Teste 1", 8, operacao.potencia(2, 3),0.0);
	}	
	@Test
	public void testDivisao() {
		/* Par�metros: mensagem, valor esperado, opera��o, delta */
		assertEquals("Teste 1", 1.25, operacao.divisao(5, 4),0.5);
		assertEquals("Teste 2", 1.25, operacao.divisao(5, 4),0);
		assertEquals("Teste 3", 1.25, operacao.divisao(5, 0),0.5);
	}
	/** Testa se o m�todo lan�a a exce��o na opera��o */
	@Test(expected=ArithmeticException.class)
	public void testExceptionDivisao() {
		assertEquals("Teste 1", 1.25, operacao.divisao(5, 0),0);
	}
}