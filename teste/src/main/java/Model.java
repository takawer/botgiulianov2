import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import com.pengrad.telegrambot.model.Update;

public class Model implements Subject{
	
	private List<Observer> observers = new LinkedList<Observer>();
	
	private List<Jogador> players = new LinkedList<Jogador>();
	
	
	private static Model uniqueInstance;
	
	private Model(){}
	
	public static Model getInstance(){
		if(uniqueInstance == null){
			uniqueInstance = new Model();
		}
		return uniqueInstance;
	}
	
	public void registerObserver(Observer observer){
		observers.add(observer);
	}
	
	public void notifyObservers(long chatId, String playersData){
		for(Observer observer:observers){
			observer.update(chatId, playersData);
		}
	}
	
	public void addJogador(Jogador jogador){
		this.players.add(jogador);
	}
	
	
	public void searchJogador(Update update){
		String game = update.message().text();
		
		//URL da API mais o nome do jogo
		URL url = new URL("https://esports.glenndehaan.com/api/players//" + game);
		// Cria um objeto da classe Jogador utils
		JogadorUtils jog = new JogadorUtils();
		// Pega a requisição em Json 
		String teste = jog.GetResponse(url);
		
		//Cria um objeto com cada requisição
		Jogador jogador = jog.deserializePlayers(teste);
		
		//Mostra o dado
		for (int i = 0;i<864;i++) {
		
				
			
				if (jogador.getPlayers().get(i).getBirth_place() != null && 
					jogador.getPlayers().get(i).getBirth_place().equals("Brazil")){
					
				this.notifyObservers(update.message().chat().id(),jogador.getPlayers().get(i).getGame());
				this.notifyObservers(update.message().chat().id(),jogador.getPlayers().get(i).getIn_game_name());
				this.notifyObservers(update.message().chat().id(),jogador.getPlayers().get(i).getReal_life_name());
				this.notifyObservers(update.message().chat().id(),jogador.getPlayers().get(i).getBirth_place());
				this.notifyObservers(update.message().chat().id(),"\n");
				
				this.notifyObservers(update.message().chat().id(),jogador.getPlayers().get(i).getId());
					
				}
			
		}
		
		
		
		
		
	}
	
	

}