package aula;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)

public class TestAreaQuadrado {
	private double a;
	private double b;
	private double resultado;
	private Operacao op;
	
	@Before
	public void setUp(){
		op = new Operacao();
	}
	
	public TestAreaQuadrado(double a, double b, double resultado){
		this.a = a;
		this.b = b;
		this.resultado = resultado;
	}
	
	@Parameterized.Parameters
	public static Collection parametros(){
		return Arrays.asList(new Object[][]{
			     {0, 0, 0},
			     {1, 1, 1},
			     {2, 0, 0},
			     {0, 2, 0} });
		
	}
	
	@Test
	public void test1() throws Exception{
		System.out.println("Testando: " + b + "*" + a);
		assertEquals(resultado , op.areaRetangulo(b , a),0);
		
	}
	
	
}
