package aula;
import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)

public class TestIsNumber {
	private boolean obj;
	private double b;
	private double resultado;
	private Operacao op;
	
	@Before
	public void setUp(){
		op = new Operacao();
	}
	
	public TestIsNumber(double a, double b, double resultado){
		this.a = a;
		this.b = b;
		this.resultado = resultado;
	}
	
	@Parameterized.Parameters
	public static Collection parametros(){
		return Arrays.asList(new Object[][] {
			{12, true},
			{12.0, true},
			{(char)12.0, false},
			{"12", false},
			{new Object(), false},
			{null, false} });
		
	}
	
	@Test(expected=Exception.class)
	public void test2() throws Exception{
		System.out.println("Testando: " + b + "*" + a);
		assertEquals(resultado , op.areaRetangulo(b , a),0);
		
	}
	
	
}